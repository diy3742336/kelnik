
# Тестовое задание Kelnik


## Установка

git clone https://gitlab.com/diy3742336/kelnik.git

docker run --rm \
--pull=always \
-v "$(pwd)":/opt \
-w /opt \
laravelsail/php82-composer:latest \
bash -c "composer install && php ./artisan sail:install --with=mysql,redis,meilisearch,mailpit,selenium"

cd kelnik && ./vendor/laravel/sail/bin/sail up

docker exec kelnik-laravel.test npm i \
docker exec kelnik-laravel.test npm run build \
docker exec kelnik-laravel.test php artisan migrate \
docker exec kelnik-laravel.test php artisan db:seed \
docker exec kelnik-laravel.test php artisan test 

## Запуск

cd kelnik && ./vendor/laravel/sail/bin/sail up 

[http://localhost](http://localhost)





