<div class="isolate inline-flex -space-x-px rounded-md shadow-sm my-3">
    {{ $data->links() }}
</div>
