@extends('layout')

@section('content')
    <div class="px-10">
        <h1 class="text-4xl my-4 ">Список статей</h1>
        <div class="mb-4 shadow-xl text-white  hover:bg-fuchsia-500 p-3 bg-fuchsia-700 rounded-lg w-44 text-center">
            <a href="{{ route(\App\Routes\ArticleRouteNames::ARTICLE_CREATE) }}" class="">Создать статью</a>
        </div>
        <table class="table">
            <thead class='text-lg pr-2'>
            <tr class="">
                <td>Название</td>
                <td>Автор</td>
                <td>Бриф</td>
            </tr>
            </thead>
            <tbody class="text-xs">
            @foreach($data->items() as $article)
                <tr class="hover:bg-amber-50">
                    <td><a class="text-blue-600 decoration-1 hover:underline"
                           href="{{route(\App\Routes\ArticleRouteNames::ARTICLE_DETAIL, ['article' => $article ])}}">{{ $article->title }}</a></td>
                    <td>{{ $article->author }}</td>
                    <td>{{ $article->short_text }}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>


    @include('components.paginate')
@endsection
