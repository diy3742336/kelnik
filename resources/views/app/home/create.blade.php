@extends('layout')

@section('content')
    <div class="p-3 bg-blue-200">
        <form action="{{ route(\App\Routes\ArticleRouteNames::ARTICLE_CREATE_FORM) }}" method="post">
            @csrf
            <div class="flex gap-3 my-2">
                <label class="w-44">Автор:</label>
                <input type="text" name="author" required placeholder="Введите автора">
            </div>
            <div  class="flex gap-3 my-2">
                <label class="w-44" >Название статьи:</label>
                <input type="text"  name="title" required placeholder="Введите заголовок статьи" >
            </div>
            <div  class="flex gap-3 my-2">
                <label class="w-44">Превью:</label>
                <input type="text" name="short_text" required placeholder="Введите превью статьи">
            </div>

            <div  class="flex gap-3 my-2">
                <label class="w-44">Текст статьи:</label>
                <textarea name="full_text" id="" cols="30" rows="10" placeholder="Полный текст статьи"></textarea>
            </div>
            <button class="bg-fuchsia-700 p-3 rounded-lg  text-white hover:bg-fuchsia-500" type="submit">Отправить</button>
        </form>
    </div>
@endsection
