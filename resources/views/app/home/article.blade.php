@extends('layout')

<a href="/">На главную</a>
@section('content')
    <h1 class="text-3xl text-blue-950 my-3 px-4">Статья: {{ $article->title }}</h1>
    <h2 class="text-2xl px-4">Автор: {{ $article->author }}</h2>
    <div class="my-4 px-4">
        <p class="my-3">Текст: </p>
        <p >{{ $article->full_text }}</p>
    </div>


@endsection
