<?php

use App\Http\Controllers\HomeController;
use App\Routes\ArticleRouteNames;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/article/{article}', [HomeController::class, 'show'])->name(ArticleRouteNames::ARTICLE_DETAIL);
Route::post('/article', [HomeController::class, 'create'])->name(ArticleRouteNames::ARTICLE_CREATE_FORM);
Route::get('/new', [HomeController::class, 'new'])->name(ArticleRouteNames::ARTICLE_CREATE);
