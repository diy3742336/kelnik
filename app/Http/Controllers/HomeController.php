<?php

namespace App\Http\Controllers;

use App\Http\Requests\Article\CreateArticleRequest;
use App\Models\Article;
use App\Service\HomeControllerService\HomeControllerService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    public function __construct(
        private readonly HomeControllerService $service
    )
    {

    }


    public function index(Request $request): Response
    {
        $data = $this->service->getTableData($request);

        return response()->view('app.home.index', compact('data'));
    }

    public function show(Article $article): Response
    {
        return response()->view('app.home.article', compact('article'));
    }

    public function new(): Response
    {
        return response()->view('app.home.create');
    }

    public function create(CreateArticleRequest $request): Response
    {
        $data = $this->service->createArticle($request);

        return response()->view('app.home.index', compact('data'));
    }

}
