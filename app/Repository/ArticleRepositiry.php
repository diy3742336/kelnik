<?php

namespace App\Repository;

use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;

class ArticleRepositiry
{

    public function getAll(): Collection
    {
        return Article::query()->orderBy('created_at','desc')->get();
    }

    public function getAllWithPaginate(int $page, $limit = 5): Collection
    {
        return $this->getAll()->forPage($page, $limit);
    }

    public function create(array $validated): bool
    {
        return Article::query()->create([
            'author' => $validated['author'] ?? null,
            'title' => $validated['title'],
            'short_text' => $validated['short_text'],
            'full_text' => $validated['full_text'],
        ])->save();
    }

}
