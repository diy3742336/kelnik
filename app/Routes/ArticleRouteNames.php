<?php

namespace App\Routes;

abstract class ArticleRouteNames
{
    public const ARTICLE_DETAIL = 'article.detail';
    public const ARTICLE_CREATE_FORM = 'article.create.form';
    public const ARTICLE_CREATE = 'article.create';
}
