<?php

namespace App\Service\HomeControllerService;

use App\Http\Requests\Article\CreateArticleRequest;
use App\Models\Article;
use App\Repository\ArticleRepositiry;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class HomeControllerService
{
    public function __construct()
    {
    }

    public  function getTableData(Request $request): LengthAwarePaginator
    {
        $limit = $request->input('limit', 5);
        $page = LengthAwarePaginator::resolveCurrentPage();

        $articles = $this->getArticleRepository()->getAllWithPaginate($page, $limit);
        return  new LengthAwarePaginator($articles, $this->getArticleRepository()->getAll()->count(), $limit, $page);
    }

    public function createArticle(CreateArticleRequest $request): LengthAwarePaginator
    {
        $this->getArticleRepository()->create($request->all());

        return $this->getTableData($request);

    }

    private function getArticleRepository(): ArticleRepositiry
    {
        return app(ArticleRepositiry::class);
    }
}
