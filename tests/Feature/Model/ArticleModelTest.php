<?php

namespace Tests\Feature\Model;

use App\Models\Article;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleModelTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_model_exists(): void
    {
        $model = Article::factory()->create();

        $this->assertModelExists($model);
    }

}
