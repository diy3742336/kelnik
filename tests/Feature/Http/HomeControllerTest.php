<?php

namespace Tests\Feature\Http;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Article;
use App\Routes\ArticleRouteNames;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic test example.
     */
    public function test_the_application_returns_a_successful_response(): void
    {

        Article::factory(10)->create();
        $response = $this->get('/');

        $response
            ->assertStatus(200)
            ->assertViewIs('app.home.index')
            ->assertSee('table')
            ->assertSeeText('Название')
            ->assertSeeText('Автор')
            ->assertSeeText('Бриф')
        ;
    }

    public function test_get_article_page(): void
    {
        Article::factory()->create([
            'id' => 1,
            'title' => 'test1',
            'author' => 'test2',
            'full_text' => 'test3',
        ]);
        $response = $this->get('/article/1');

        $response
            ->assertStatus(200)
            ->assertViewIs('app.home.article')
            ->assertSeeText('test1')
            ->assertSeeText('test2')
            ->assertSeeText('test3')
        ;
    }

    public function test_get_404_if_article_not_exist(): void
    {
        Article::factory()->create([
            'id' => 1,
            'title' => 'test1',
            'author' => 'test2',
            'full_text' => 'test3',
        ]);
        $response = $this->get('/article/3');

        $response
            ->assertStatus(404);
    }

    public function test_create_method(): void
    {
        $fakePost = [
            'author' => fake()->name,
            'title' => fake()->domainName(),
            'short_text' => fake()->text(300),
            'full_text' => fake()->text,
        ];

        $response = $this->post(route(ArticleRouteNames::ARTICLE_CREATE_FORM), $fakePost);

        $response->assertStatus(200);
    }

    public function test_create_method_author_less_100_length(): void
    {
        $fakePost = [
            'author' => fake()->text(300),
            'title' => fake()->domainName(),
            'short_text' => fake()->text(300),
            'full_text' => fake()->text,
        ];

        $response = $this->post(route(ArticleRouteNames::ARTICLE_CREATE_FORM), $fakePost);

        $response->assertStatus(302);
    }

    public function test_create_method_title_not_set(): void
    {
        $fakePost = [
            'author' => fake()->name(),
            'short_text' => fake()->text(300),
            'full_text' => fake()->text,
        ];

        $response = $this->post(route(ArticleRouteNames::ARTICLE_CREATE_FORM), $fakePost);

        $response->assertStatus(302);
    }

    public function test_create_method_short_text_less_500_length(): void
    {
        $fakePost = [
            'title' => fake()->domainName(),
            'short_text' => fake()->text(600),
            'full_text' => fake()->text,
        ];

        $response = $this->post(route(ArticleRouteNames::ARTICLE_CREATE_FORM), $fakePost);

        $response->assertStatus(302);
    }
}
